Meteor.linkFacebook = function (options, callback) {
  // support a callback without options
  if (!callback && typeof options === "function") {
    callback = options;
    options  = null;
  }

  var fbLoginSuccess = function (data) {
    data.cordova = true;
    Meteor.call('linkFB', data.authResponse, callback);
  };

  if (typeof facebookConnectPlugin != "undefined" && Meteor.settings) {
    facebookConnectPlugin.getLoginStatus(
      function (response) {
        if (response.status != "connected") {
          facebookConnectPlugin.login(Meteor.settings.public.facebook.permissions,
            fbLoginSuccess,
            function (error) {
              callback(error)
            }
          );
        } else {
          fbLoginSuccess(response);
        }
      },
      function (error) {
        callback("" + error)
      }
    );
  }
};

Meteor.loginWithFacebook = function (options, callback) {
  // support a callback without options
  if (!callback && typeof options === "function") {
    callback = options;
    options  = null;
  }

  var credentialRequestCompleteCallback = Accounts.oauth.credentialRequestCompleteHandler(callback);

  var fbLoginSuccess = function (data) {
    data.cordova = true;

    Accounts.callLoginMethod({
      methodArguments: [data],
      userCallback:    callback
    });
  };

  if (typeof facebookConnectPlugin != "undefined" && Meteor.settings) {
    facebookConnectPlugin.getLoginStatus(
      function (response) {
        if (response.status != "connected") {
          facebookConnectPlugin.login(Meteor.settings.public.facebook.permissions,
            fbLoginSuccess,
            function (error) {
              callback("" + error)
            }
          );
        } else {
          fbLoginSuccess(response);
        }
      },
      function (error) {
        callback("" + error)
      }
    );
  } else {
    Facebook.requestCredential(options, credentialRequestCompleteCallback);
  }
};


Meteor.loginWithFacebookSplash = function (options, callback) {
  showFacebookSplash();
  Meteor.loginWithFacebook(options, function (err) {
    console.log("LOGIN WITH FACEBOOK", err);
    callback(err);
    hideFacebookSplash();
  })
};

showFacebookSplash = function () {
  var elemDiv   = document.createElement('div');
  var fbIconDiv = document.createElement('h1');

  fbIconDiv.innerText     = "Facebook";
  fbIconDiv.style.cssText = "position: absolute; top:50%; left: 50%; color:white; margin-right: -50%; transform: translate(-50%, -50%);";

  elemDiv.style.cssText = 'position:absolute;width:100%;height:0%;z-index:100;background:#3B5998;bottom:0; left:0';
  elemDiv.className     = "extend-top";
  elemDiv.id            = "fb-oauth-splash";

  elemDiv.appendChild(fbIconDiv);

  document.body.appendChild(elemDiv);
};

var hideFacebookSplash = function () {
  var el = $('.extend-top');
  el.removeClass('extend-top');
  el.addClass('shrink-bottom');
};