Package.describe({
  name: 'mirrorcell:facebook-auth',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Cordova.depends({
  'cordova-plugin-facebook4': '1.4.0'
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.1');

  api.use(['ecmascript',
    'meteor-base',
    'accounts-base',
    'facebook',
    'btafel:accounts-facebook-cordova',
    'check',
    "http"
  ]);

  api.imply(['check', 'accounts-base', 'facebook']);

  api.addFiles('lib/fb_native_logout.js', 'web.cordova');

  api.addFiles(['lib/fb_client.js', 'lib/styles.css'], 'client');
  api.addFiles('lib/fb_server.js', 'server');
  api.addFiles('lib/fb_shared.js', ['client', 'server']);
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('mirrorcell:facebook-auth');
  api.addFiles('facebook-auth-tests.js');
});
